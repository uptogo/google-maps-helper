<?php

namespace GoogleMapsHelper;

use \GoogleMapsHelper\Address;
use \GoogleMapsHelper\Route;

class GoogleMapsHelper {
  private $apiKey;
  private $urlBase = 'https://maps.googleapis.com/maps/api/';

  public function __construct($apiKey) {
    $this->setApiKey($apiKey);
  }

  public function setApiKey($apiKey) {
    $this->apiKey = $apiKey;
  }

  private function getPermanentUrlArgs() {
    return 'json?key=' . $this->apiKey . '&';
  }

  private function getNetworkData($urlComplement) {
    return json_decode(file_get_contents(
      $this->urlBase . $urlComplement
    ), true);
  }

  public function geocode($address) {
    $networkData = $this->getNetworkData(
      'geocode/' . $this->getPermanentUrlArgs() .
      'address=' . urlencode($address)
    );

    if ($networkData['status'] !== 'OK') {
      throw new \Exception($networkData['error_message']);
    }

    $results = Array();
    foreach ($networkData['results'] as $result) {
      $results[] = new Address($result);
    }
    return $results;
  }

  public function directions($origin, $dest) {
    $networkData = $this->getNetworkData(
      'directions/' . $this->getPermanentUrlArgs() .
      'origin=' . $origin->getGeolocation()->getFormatted() .
      '&destination=' . $dest->getGeolocation()->getFormatted()
    );

    if ($networkData['status'] !== 'OK') {
      throw new \Exception($networkData['error_message']);
    }

    $results = Array();
    foreach ($networkData['routes'] as $result) {
      $results[] = new Route($origin, $dest, $result);
    }
    return $results;
  }

  public static function getDirectDistance($origin, $dest) {
    $latFrom = deg2rad($origin->getGeolocation()->getLat());
    $lonFrom = deg2rad($origin->getGeolocation()->getLng());
    $latTo = deg2rad($dest->getGeolocation()->getLat());
    $lonTo = deg2rad($dest->getGeolocation()->getLng());
    $lonDelta = $lonTo - $lonFrom;
    $a = pow(cos($latTo) * sin($lonDelta), 2) +
    pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) *
    cos($lonDelta), 2);
    $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo)
    * cos($lonDelta);
    $angle = atan2(sqrt($a), $b);
    return $angle * 6371000;
  }
}