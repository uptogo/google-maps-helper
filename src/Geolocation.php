<?php

namespace GoogleMapsHelper;

class Geolocation  {
    private $lat;
    private $lng;

    public function __construct($lat, $lng) {
      $this->setLat($lat);
      $this->setLng($lng);
    }

    public function getLat() {
      return $this->lat;
    }

    public function setLat($lat) {
      $this->lat = $lat;
    }

    public function getLng() {
      return $this->lng;
    }

    public function setLng($lng) {
      $this->lng = $lng;
    }

    public function getFormatted() {
      return ((string) $this->lat) . ',' . ((string) $this->lng);
    }
}