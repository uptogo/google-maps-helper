<?php

namespace GoogleMapsHelper;

use \GoogleMapsHelper\Address;

class Route {
  private $origin;
  private $destination;
  private $distance;
  private $duration;
  private $polyline;

  public function __construct($origin, $destination, $routeComponents) {
    $this->setOrigin($origin);
    $this->setDestination($destination);
    $this->deflateRouteComponents($routeComponents);
  }

  private function deflateRouteComponents($routeComponents) {
    $distance = 0;
    $duration = 0;
    foreach($routeComponents['legs'] as $legs) {
      $distance += $legs['distance']['value'];
      $duration += $legs['duration']['value'];
    }
    $this->setDistance($distance);
    $this->setDuration($duration);
    $this->setPolyline($routeComponents['overview_polyline']['points']);
  }

  public function getOrigin() {
    return $this->origin;
  }

  public function setOrigin($origin) {
    $this->origin = $origin;
  }

  public function getDestination() {
    return $this->destination;
  }

  public function setDestination($destination) {
    $this->destination = $destination;
  }

  public function getDistance() {
    return $this->distance;
  }

  public function setDistance($distance) {
    $this->distance = $distance;
  }

  public function getDuration() {
    return $this->duration;
  }

  public function setDuration($duration) {
    $this->duration = $duration;
  }

  public function getPolyline() {
    return $this->polyline;
  }

  public function setPolyline($polyline) {
    $this->polyline = $polyline;
  }
}