<?php

namespace GoogleMapsHelper;

use \GoogleMapsHelper\Geolocation;

class Address {
  private $formatted;
  private $street;
  private $number;
  private $district;
  private $zipCode;
  private $city;
  private $state;
  private $country;
  private $geolocation;

  public function __construct($addressComponents = null) {
    if ($addressComponents !== null) {
      $this->deflateAddressComponents($addressComponents);
    }
  }

  private function deflateAddressComponents($addressComponents) {
    foreach ($addressComponents as $componentIndex => $component) {
      if ($componentIndex === 'address_components') {
        if (is_array($component)) {
          foreach ($component as $attrs) {
            if (is_array($attrs)) {
              foreach ($attrs as $attrIndex => $attr) {
                if ($attrIndex === 'types') {
                  foreach ($attr as $type) {
                    switch ($type) {
                      case 'route':
                      $this->setStreet($attrs['long_name']);
                      break;
                      case 'street_number':
                      $this->setNumber($attrs['long_name']);
                      break;
                      case 'sublocality':
                      $this->setDistrict($attrs['long_name']);
                      break;
                      case 'postal_code':
                      $this->setZipCode($attrs['long_name']);
                      break;
                      case 'administrative_area_level_2':
                      $this->setCity($attrs['long_name']);
                      break;
                      case 'administrative_area_level_1':
                      $this->setState($attrs['short_name']);
                      break;
                      case 'country':
                      $this->setCountry($attrs['short_name']);
                      break;
                    }
                  }
                }
              }
            }
          }
        }
      } else if ($componentIndex === 'formatted_address') {
        $this->setFormatted($component);
      } else if ($componentIndex === 'geometry') {
        $this->setGeolocation(new Geolocation(
          $component['location']['lat'],
          $component['location']['lng']
        ));
      }
    }
  }

  public function getFormatted() {
    return $this->formatted;
  }

  public function setFormatted($formatted) {
    $this->formatted = $formatted;
  }

  public function getStreet() {
    return $this->street;
  }

  public function setStreet($street) {
    $this->street = $street;
  }

  public function getNumber() {
    return $this->number;
  }

  public function setNumber($number) {
    $this->number = $number;
  }

  public function getDistrict() {
    return $this->district;
  }

  public function setDistrict($district) {
    $this->district = $district;
  }

  public function getZipCode() {
    return $this->zipCode;
  }

  public function setZipCode($zipCode) {
    $this->zipCode = $zipCode;
  }

  public function getCity() {
    return $this->city;
  }

  public function setCity($city) {
    $this->city = $city;
  }

  public function getState() {
    return $this->state;
  }

  public function setState($state) {
    $this->state = $state;
  }

  public function getCountry() {
    return $this->country;
  }

  public function setCountry($country) {
    $this->country = $country;
  }

  public function getGeolocation() {
    return $this->geolocation;
  }

  public function setGeolocation($geolocation) {
    $this->geolocation = $geolocation;
  }
}